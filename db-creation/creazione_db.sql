SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Database TECNO LIFE
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema TecnoLife
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `TecnoLife` DEFAULT CHARACTER SET utf8 ;
USE `TecnoLife` ;

-- -----------------------------------------------------
-- Table `TecnoLife`.`prodotto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `TecnoLife`.`prodotto` (
  `codProdotto` INT NOT NULL AUTO_INCREMENT,
  `prezzoUnitario` FLOAT NOT NULL,
  `quantità` INT NOT NULL,
  `nomeProdotto` VARCHAR(100) NOT NULL,
  `descrizione` VARCHAR(10000) NOT NULL,
  `imgProdotto` VARCHAR(100) NOT NULL,
  `offerta` INT NOT NULL,
  PRIMARY KEY (`codProdotto`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `TecnoLife`.`categoria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `TecnoLife`.`categoria` (
  `codCategoria` INT NOT NULL AUTO_INCREMENT,
  `nomeCategoria` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`codCategoria`)
)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `TecnoLife`.`carrello`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `TecnoLife`.`carrello` (
  `numeroProdotto` INT NOT NULL AUTO_INCREMENT,
  `codProdotto` INT NOT NULL,
  `idUtente`  INT NOT NULL,
  `prezzo`  INT NOT NULL,
  `nomeProdotto` VARCHAR(100) NOT NULL,
  `imgProdotto` VARCHAR(100) NOT NULL,
  `quantità`  INT NOT NULL,
  `inclusione` INT NOT NULL,
  PRIMARY KEY (`numeroProdotto`)
)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `TecnoLife`.`utente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `TecnoLife`.`utente` (
  `idUtente` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(100) NOT NULL,
  `password` VARCHAR(100) NOT NULL,
  `amministratore` INT NOT NULL,
  PRIMARY KEY (`idUtente`)
)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `TecnoLife`.`notifiche`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `TecnoLife`.`notifiche` (
  `codice` INT NOT NULL AUTO_INCREMENT,
  `idUtente` INT NOT NULL,
  `tipologia` INT NOT NULL,
  `contenuto` VARCHAR(1000) NOT NULL, 
  PRIMARY KEY (`codice`)
)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `TecnoLife`.`prodotto_ha_categoria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `TecnoLife`.`prodotto_ha_categoria` (
  `prodotto` INT NOT NULL,
  `categoria` INT NOT NULL,
  PRIMARY KEY (`prodotto`, `categoria`),
  INDEX `fk_prodotto_has_categoria_categoria1_idx` (`categoria` ASC),
  INDEX `fk_prodotto_has_categoria_prodotto1_idx` (`prodotto` ASC),
  CONSTRAINT `fk_prodotto_has_categoria_prodotto1`
    FOREIGN KEY (`prodotto`)
    REFERENCES `TecnoLife`.`prodotto` (`codProdotto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_prodotto_has_categoria_categoria1`
    FOREIGN KEY (`categoria`)
    REFERENCES `TecnoLife`.`categoria` (`codCategoria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
