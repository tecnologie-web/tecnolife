INSERT INTO `prodotto` (`codProdotto`, `prezzoUnitario`, `quantità`, `nomeProdotto`,`descrizione`, `imgProdotto`, `offerta`) VALUES


(1, '39.99', 200, 'AUKEY Smartwatch','Smartwatch Full Touch 320p Schermo Orologio Fitness Activity Tracker, Impermeabil IP68, Cardiofrequenzimetro, Cronometro Contapassi, Notifiche Messaggi, Controllo della Musica', 'AUKEY-Smartwatch.jpg', 1),
(2, '32.99', 150, 'Xiaomi Band 5','Orologio Fitness Tracker Uomo Donna Cardiofrequenzimetro da Polso Contapassi Smartband Sportivo Activity Tracker Versione Globale', 'Xiaomi-Band-5.jpg', 0),
(3, '151', 133, 'Honor Magic Watch 2', 'Smartwatch,GPS 5ATM Impermeabile Orologio Bluetooth Smart Monitor di Frequenza Cardiaca, Stress e Spo2,Smart Watch Donne Uomo, Per Android (Nero 46mm)', 'Honor-Magic-Watch-2.jpg', 1),
(4, '49.99', 45, 'UMIDIGI Smartwatch uomo','Orologio Fitness Smart Watch con GPS Integrato, Quadrante Personalizzato, Impermeabile 5ATM, Cardiofrequenzimetro da Polso Contapassi Sportivo Activity Tracker per Android iOS', 'UMIDIGI-Smartwatch-uomo.jpg', 0),
(5, '219', 80, 'Apple Watch Series 3',' Apple watch dotato di GPS, Cardiofrequenzimetro ottico, Digital Crown, S3 con processore dual‑core, Accelerometro e,giroscopio, Swimproof,watchOS 5, Cassa in alluminio', 'Apple-Watch-Series-3.jpg', 0),
(6, '459', 111, 'Apple Watch Series 6','Con il nuovo modello GPS rispondi a chiamate e messaggi dall’orologio. Puoi misurare l’ossigeno nel sangue con un nuovo sensore. Puoi controllare il ritmo cardiaco con l’app ECG. Il display è 2,5 volte più luminoso alla luce del giorno. Con Apple Watch misuri ogni giorno quanto ti muovi, e i progressi compiuti li controlli nell’app Fitness su iPhone', 'Apple-Watch-Series-6.jpg', 0),
(7, '106.25', 40, 'Amazfit GTS Smartwatch','Smartwatch Orologio Intelligente Fitness 5 ATM Impermeabile Durata Batteria Fino a 14 Giorni con GPS, 12 Modalità di Sport, Display del Quadrante in Vetro 3D Contapassi per Sport', 'Amazfit-GTS-Smartwatch.jpg', 0),
(8, '42.99', 15, 'Motast Smartwatch Donna',' Dotato di Contapassi, Cardiofrequenzimetro da Polso, Smartband Sportivo Activity Tracker Cronometro, Notifiche Messaggi, Controller Fotocamera Musicale', 'Motast-Smartwatch-Donna.jpg', 1),
(9, '47.99', 93, 'HUAWEI Band 4 Pro','Touchscreen AMOLED 0.95”, Monitoraggio Battito Cardiaco, Monitoraggio Scientifico del Sonno, GPS Integrato, Resistente all’Acqua, Graphite Black', 'HUAWEI-Band-4-Pro.jpg', 0),
(10, '46', 50, 'Canmixs Smartwatch','Orologio Fitness Tracker Uomo Donna, Bluetooth Smart Watch Cardiofrequenzimetro Da Polso Contapassi Conta Calorie Impermeabile Ip67 Sportivo Activity Tracker Per Android Ios', 'Canmixs-Smartwatch.jpg', 0),

  
(11, '650', 50, 'Iphone 12pro','Display Super Retina XDR da 6,1" Ceramic Shield, più duro di qualsiasi vetro per smartphone 5G per download velocissimi e streaming ad alta qualità A14 Bionic, il chip più veloce mai visto su uno smartphone Evoluto sistema a doppia fotocamera da 12MP (ultra-grandangolo, grandangolo) con modalità Notte, Deep Fusion, Smart HDR 3 e registrazione video HDR a 4K in Dolby Vision', 'iphone-12-pro.jpg', 0),
(12, '389.99', 75, 'Samsung Galaxy M51','Smartphone, Display 6.7" Super AMOLED Plus, 4 Fotocamere, 128GB Espandibili, RAM 6 GB, Batteria 7000 mAh, 4G, Dual Sim, Android 10, [Versione Italiana], Bianco, Esclusiva Amazon', 'Samsung-Galaxy-M51.jpg', 0),
(13, '215', 30, 'Huawei P40 Lite','Sistema Operativo Android. Fotocamera da 48 + 8 + 2 + 2 mp apertura grandangolo f/1.8, obiettivo ultra grandangolare, obiettivo macro, lente bokeh. Memoria da 128 GB espandibile con microsd da 256 GB', 'Huawei-P40-Lite.jpg', 1),
(14, '279', 50, 'Samsung Galaxy M31','martphone, Display 6.4" Super AMOLED, 4 Fotocamere Posteriori, 64 GB Espandibili, RAM 6 GB, 4G, Dual Sim, Android 10, [Versione Italiana], Red, ', 'Samsung-Galaxy-M31.jpg', 1),
(15, '132.90', 250, 'Samsung Galaxy A20e','Smartphone, Display 5.8" HD+, 32 GB Espandibili, RAM 3 GB, Batteria 3000 mAh, 4G, Dual SIM, Android 9 Pie, [Versione Italiana], Blue', 'Samsung-Galaxy-A20e.jpg', 0),
(16, '269.90', 120, 'Xiaomi POCO X3 NFC','Smartphone 6+128GB, display 6,67” FHD+, Snapdragon 732G, 64MP AI Quad-Camera, batteria 5160mAh, Grigio (Shadow Gray)', 'Xiaomi-POCO-X3-NFC.jpg', 1),
(17, '149.90', 130, 'Xiaomi Redmi 9C','Smartphone 3GB 64GB 6.53" HD+ Dot Drop display 5000mAh (typ) AI Face Unlock 13 MP AI Triple telecamera [Versione globale] Blu', 'Xiaomi-Redmi-9C.jpg', 0),
(18, '649.50', 111, 'Apple iPhone SE','Display Retina HD da 4,7. Resistente alla polvere e all’acqua (1 metro fino a 30 minuti, IP67). Fotocamera da 12MP con modalità Ritratto, Illuminazione ritratto, Controllo profondità e registrazione video 4K', 'Apple-iPhone-SE.jpg', 0),
(19, '199', 23, 'Samsung Galaxy A30s','Smartphone, Display 6.4" Super AMOLED, 128 GB Espandibili, RAM 4 GB, Batteria 4000 mAh, 4G, Dual SIM, Android 9 Pie [Versione Italiana], Black', 'Samsung-Galaxy-A30s.jpg', 0),

(20, '144.9', 98, 'Huawei P Smart','Display: 6,21 ", 1080 x 2340 pixel. Processore: Kirin 710 2.2GHz. Fotocamera: doppia, 13MP + 2MP. Batteria: 3400 mAh', 'Huawei-P-Smart.jpg', 1),



(21, '1600', 80, 'Huawei MateBook X Pro Laptop','Laptop, Touchscreen FullView Ultrabook da 13.9 Pollici, Intel i7 10510U, 16 GB RAM, 1 TB SSD, NVIDIA GeForce MX250, Huawei Share, Windows 10 Home, Grigio', 'Huawei-MateBook-X-Pro-Laptop.jpg', 0),
(22, '450', 38, 'Chuwi CoreBook Pro Laptop','Laptop Ultrabook 13 pollici Win 10 Notebook Portatile Intel Core i3-6157U fino a 2,4 GHz 8 GB RAM 256 GB SSD 2160 * 1440 2K, Type-C 2.4G/5G WiFi', 'Chuwi-CoreBook-Pro-Laptop.jpg', 1),
(23, '369.99', 77, 'TECLAST F15S Notebook','Pc Portatile 15.6 Pollici, Intel Apollo Lake Processore, fino a 2.4 GHz Computer Portatile, 4K, 1920 x 1080, Windows 10, 8GB +128GB, USB 3.0, Mini HDMI, Bluetooth 4.2', 'TECLAST-F15S-Notebook.jpg', 0),
(24, '423.50', 50, 'Notebook Windows10','PC Portatile da Intel Celeron J4115 Quad Core Fino a 2.5Ghz, 14.1 pollici Ultrabook con TIPO C USB3.0 impronta digitale Sblocca la retroilluminazione della tastiera(8GB+256GB SSD)', 'Notebook-Windows10.jpg', 0),
(25, '450', 120, 'Acer Chromebook 314 CB314-1H-C2W1','Pc Portatile con Processore Intel Celeron N4000, Ram 4GB DDR4, eMMC 64 GB, Display 14" Full HD LED LCD, Scheda Grafica Intel, Chrome OS, Silver, [CB]', 'Acer-Chromebook-314-CB314-1H-C2W1.jpg', 1),
(26, '1699.99', 80, 'HP-Gaming OMEN 15-ek0015nl','Intel Core i7-10750H, RAM 16 GB, SSD 512 GB, NVIDIA GeForce RTX 2060 6 GB, Windows 10 Home, Schermo 15.6" FHD IPS 144 Hz, Bang&Olufsen, USB-C, HDMI, RJ-45, Nero', 'HP-Gaming-OMEN-15-ek0015nl.jpg', 0),
(27, '719', 30, 'Portatile Lenovo V15','cpu Intel i5 10th GEN. 4 core, Notebook 15.6" Display FHD 1920 x 1080 Pixels 39,6 cm, DDR4 8 GB , SSD 256 GB , webcam, Wi-fi, Bt, Win10 Pro', 'Portatile-Lenovo-V15.jpg', 0),


(28, '170', 120, 'Lenovo Tab M10','Tablet, Display 10.1" HD, Processore MediaTek Helio P22T, Storage 32 GB Espandibile fino ad 1 TB, RAM 2 GB, WIFI+Bluetooth 5.0, 4G LTE, 2 Speaker, Android 10, Iron Grey', 'Lenovo-Tab-M10.jpg', 1),
(29, '129.90', 50, 'HUAWEI MatePad T 10','display da 9.7", RAM da 2 GB, Memoria Interna da 32 GB, Wi-Fi, Processore Octa-Core, sistema operativo EMUI 10 con Huawei Mobile Services (HMS), Dual-speaker, Deepsea Blue', 'HUAWEI-MatePad-T-10.jpg', 0),
(30, '459', 80, 'Apple iPad mini','Sensore di impronte digitali Touch ID. Fotocamera posteriore da 8MP, fotocamera anteriore FaceTime HD da 7MP. Altoparlanti stereo. Fino a 10 ore di autonomia', 'Apple-iPad-mini.jpg', 0),
(31, '1399', 91, 'Apple iPad Pro 11','Display Liquid Retina edge-to-edge da 11" con tecnologia ProMotion Grandangolo da 12MP, ultra-grandangolo da 10MP, scanner LiDAR. Fotocamera anteriore TrueDepth da 7MP Audio a quattro altoparlanti e cinque microfoni di qualità professionale','Apple-iPad-Pro-11.jpg', 0),
(32, '789', 21, 'Samsung Galaxy Tab S7 ','Tablet S Pen, Snapdragon 865 Plus, Display 11.0" WQXGA, 128GB Espandibili fino a 1TB, RAM 6GB, Batteria 8.000 mAh, LTE, Android 10, Mystic Black', 'Samsung-Galaxy-Tab-S7.jpg ', 0),
(33, '199', 124, 'Samsung Galaxy Note 10','Versione 2014 Tablet, 10,1 pollici, Toucshcreen, 3GB RAM, 16GB Memoria Interna, Camera 8 MP, WiFi, Android 4.3, colore: Nero', 'Samsung-Galaxy-Note-10.jpg', 1),


(34, '200', 23, 'Bose QuietComfort 35','Tecnologia di riduzione del rumore di qualità per un silenzio ancora più silenzioso e un suono ottimo
Pairing via Bluetooth ed NFC con istruzioni vocali per connessioni wireless più facili', 'Bose-QuietComfort-35.jpg', 0),
(35, '99.95', 111, 'Beats EP Cuffie con filo','Suono calibrato magistralmente. Design resistente e leggerissimo, rinforzato con acciaio inossidabile. Rispondi alle chiamate e controlla la musica sui tuoi dispositivi iOS con il microfono integrato nel cavo RemoteTalk', 'Beats-EP-Cuffie-con-filo.jpg', 1),
(36, '31',56, 'Sony WH-CH510','Cuffie wireless on-ear, Compatibile con Google Assistant e Siri, Batteria fino a 35 ore, Bluetooth, Nero', 'Sony-WH-CH510.jpg', 1),
(37, '20.99', 50, 'GAMURRY Auricolari','Cancella suono stereo dinamico: Premium Suono Qualità Porterà un suono cristallino e dinamico. Design Ergonomico. Microfono incorporato ', 'GAMURRY-Auricolari.jpg', 0),
(38, '179', 14, 'Sony-WF-1000XM3','Cuffie completamente wireless con HD Noise Cancelling, Compatibili con Google Assistant, Alexa e Siri, iOS/Android, Bluetooth, NFC, Batteria fino a 32 ore, Nero', 'Sony-WF-1000XM3.jpg', 0),
(39, '59.99', 114, 'AUKEY Cuffie Bluetooth','ancellazione Attiva del Rumore, Auricolari Senza Fili con 4 Microfoni, 35 Ore di Riproduzione, IPX5 Impermeabili, Type-C Cuffie Wireless per iPhone, Samsung', 'AUKEY-Cuffie-Bluetooth.jpg', 0),


(40, '550', 98, 'HP – PC M01-F0028nl Desktop','AMD Ryzen 3 3200G, RAM 8 GB, SSD 256 GB, Grafica AMD Radeon Vega 8, Windows 10 Home, Lettore DVD, Lettore Micro SD, Wi-Fi, Bluetooth, HDMI, VGA, USB, RJ-45, Nero', 'HP–PC-M01-F0028nl-Desktop.jpg', 0),
(41, '299', 26, 'CHUWI CoreBox Mini PC Windows','Intel Core i5-5257U Mini Desktop pc, 8GB DDR3 ,256GB SSD,Expandable 2TB 2.5 Pollici HDD 2.4GHz, 5GHz Dual WiFi', 'CHUWI-CoreBox-Mini-PC-Windows.jpg', 0),
(42, '1300', 50, 'Apple-iMac','Intel Core i5, 3.2 GHz. RAM 16 GB . 1000GB HDD. Display altadefinizione fino a 4k', 'Apple-iMac.jpg', 0),
(43, '900', 26, 'PC Extreme Gaming Desktop','Intel Six Core i5 9400 UP 4,10 GHZ. Grafica Nvidia Gtx 1660 6GB. Ram 16GB DDR4. SSD 480 GB. HD 1TB. Wi-Fi Usb 3.0 Hdmi. Windows 10 Pro', 'PC-Extreme-Gaming-Desktop.jpg', 0),
(44, '719', 100, 'DILC Business 7 Pc Desktop','Intel i7-9700F 3.00 ghz Ram 8 gb Ssd 240 gb + Hard Disk 1 tb GT 710 2gb WiFi Masterizzatore 450W 80+ Licenza Windows 10 PRO', 'DILC-Business-7-Pc-Desktop.jpg', 0),


(45, '179', 55, 'iRobot Roomba 605',' Robot Aspirapolvere, Sistema di Pulizia ad Alte Prestazioni, Adatto a Pavimenti e Tappeti, Ottimo per i Peli degli Animali Domestici, 33 watt, Autonomia fino a 1 ora, Bianco', 'iRobot-Roomba-605.jpg', 0),
(46, '399', 85, 'iRobot Roomba 971','Robot aspirapolvere WiFi, Power-Lifting, Dirt Detect, Adatto per peli di Animali Domestici, Tecnologia Imprint, Sistema Pulizia 3 Fasi, programmabile con App, Argento', 'iRobot-Roomba-971.jpg', 0),
(47, '159', 130, 'Robot Aspirapolvere Mini','6D Sensore di Collisione WiFi/App/Alexa Ricaricabile Automaticamente Scatola di Polvere 500ml, ideale per peli di animali, moquette e pavimento duro, Lefant-M201', 'Robot-Aspirapolvere-Mini.jpg', 1),
(48, '149.99', 25, 'Ariete 2718 Xclean Robot ','Robot Aspirapolvere, Partenza Ritardata, Filtro HEPA, Autonomia 1.5 h, 65 dB, diametro: 30 cm, Capacità 300 ml, 25W, Nero
', 'Ariete-2718-Xclean-Robot.jpg', 0);

ALTER TABLE `prodotto`
  MODIFY `codProdotto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

INSERT INTO `categoria` (`codCategoria`, `nomeCategoria`) VALUES
(1, 'SmartWatch'),
(2, 'SmartPhone'),
(3, 'PC'),
(4, 'Desktop PC'),
(5, 'HeadPhone'),
(6, 'Tablet'),
(7, 'Robot');

ALTER TABLE `categoria`
  MODIFY `codCategoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

INSERT INTO `utente` (`idUtente`, `username`, `password`, `amministratore`) VALUES
(1, 'riccardo.bacca@studio.unibo.it', 'pass2020', 1),
(2, 'stefano.camporesi@studio.unibo.it', 'pass2020', 1);

INSERT INTO `prodotto_ha_categoria` (`prodotto`, `categoria`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 2),
(12, 2),
(13, 2),
(14, 2),
(15, 2),
(16, 2),
(17, 2),
(18, 2),
(19, 2),
(20, 2),
(21, 3),
(22, 3),
(23, 3),
(24, 3),
(25, 3),
(26, 3),
(27, 3),
(28, 6),
(29, 6),
(30, 6),
(31, 6),
(32, 6),
(33, 6),
(34, 5),
(35, 5),
(36, 5),
(37, 5),
(38, 5),
(39, 5),
(40, 4),
(41, 4),
(42, 4),
(43, 4),
(44, 4),
(45, 7),
(46, 7),
(47, 7),
(48, 7);
