<?php
    require_once("bootstrap.php");

    $messaggio = "";

    $templateParams["carrello"] = $dbh -> getCarrello($_SESSION["idUtente"]);
    $templateParams["sommaTotale"]=0;
    $templateParams["inclusione"] = $dbh -> getInclusione($_SESSION["idUtente"]);
    if(empty($templateParams["carrello"])){
        $messaggio="Non sono presenti articoli nel carrello";
        header("location: carrello.php?msg=".$messaggio);
    }else if(empty($templateParams["inclusione"])){
        $messaggio= "Selezionare almeno un prodotto per effettuare l'acquisto";
        header("location: carrello.php?msg=".$messaggio);
    }else{
        foreach($templateParams["carrello"] as $prodottoCarrello){
            if(isset($_POST["presente_".$prodottoCarrello["codProdotto"]]) && $_POST["presente_".$prodottoCarrello["codProdotto"]]=="on"){
                $dbh->setIncludedShoppingCart($prodottoCarrello["codProdotto"], $_SESSION["idUtente"]);

                $prodottoMagazzino = $dbh->getProduct($prodottoCarrello["codProdotto"]);

                if(!isset($_POST["quantità_".$prodottoCarrello["codProdotto"]]) || $_POST["quantità_".$prodottoCarrello["codProdotto"]] < 0) {
                    $messaggio .= "Quantità prodotto errata per $prodottoCarrello[nomeProdotto]!!";
                } else if($prodottoMagazzino[0]["quantità"] < $_POST["quantità_".$prodottoCarrello["codProdotto"]]){
                    $var = $prodottoMagazzino[0]["quantità"];
                    if($var==0){
                        $messaggio .= "Prodotto temporaneamente esaurito !!";
                    } else {
                        $messaggio .= " Quantità prodotto eccessiva per $prodottoCarrello[nomeProdotto], massimo $var !!";
                    } 
                }
                $templateParams["sommaTotale"]+=($prodottoCarrello["prezzo"]*$_POST["quantità_".$prodottoCarrello["codProdotto"]]);
            } else {
                $dbh->setNotIncludedShoppingCart($prodottoCarrello["codProdotto"], $_SESSION["idUtente"]);
            }
            if($messaggio==""){
                $dbh->setQuantityShoppingCart($prodottoCarrello["codProdotto"], $_POST["quantità_".$prodottoCarrello["codProdotto"]], $_SESSION["idUtente"]);
            }
        }
        if($messaggio==""){
            header("location: Pagamento.php?tot=".$templateParams["sommaTotale"]);
        } else {
            header("location: carrello.php?msg=".$messaggio);
        }
    }
?>