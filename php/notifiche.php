<?php
require_once("bootstrap.php");

if(isset($_GET["action"]) && $_GET["action"]==1){
    $dbh->deleteUserNotify($_SESSION["idUtente"], $_GET["id"]);
}
$templateParams["notifiche"]=$dbh->getUserNotify($_SESSION["idUtente"]);

$templateParams["nome"] = "template/Notifiche.php";
$templateParams["titolo"] = "Notifiche Utente";
require("template/Base.php");
?>