<?php
    require_once("bootstrap.php");
    $categoria= $dbh->getCategory(($_GET["ricerca"]));
    $templateParams["ordinamento"]= $_GET["ricerca"];
    if($_GET["action"]=="1"){
        if(isset($categoria) && count($categoria)==1){
            $templateParams["ricerca"] = $dbh->orderASCPriceByCategory($categoria[0]);
        }else{
            $templateParams["ricerca"] = $dbh->orderASCPrice($_GET["ricerca"]);
        }

    }
    if($_GET["action"]=="2"){
        if(isset($categoria)){
            $templateParams["ricerca"] = $dbh->orderDESCPriceByCategory($categoria[0]);
        }else{
            $templateParams["ricerca"] = $dbh->orderDESCPrice($_GET["ricerca"]);
        }
    }
    if($_GET["action"]=="3"){
        if(isset($categoria)){
            $templateParams["ricerca"] = $dbh->orderByBestSellersCategory($categoria[0]);
        }else{
            $templateParams["ricerca"] = $dbh->orderByBestSellers($_GET["ricerca"]);
        }
    }
    if($_GET["action"]=="4"){
        if(isset($categoria)){
            $templateParams["ricerca"] = $dbh->getProductOnOfferByCategory($categoria[0]);
        }else{
            $templateParams["ricerca"] = $dbh->getProductOnOffer($_GET["ricerca"]);
        }
    }
    if($_GET["action"]=="5"){
        if(isset($categoria)){
            $templateParams["ricerca"] = $dbh->getProdByCategories($categoria[0]);
        }else{
            $templateParams["ricerca"] = $dbh->getProductSearch($_GET["ricerca"]);
        }
    }
    $templateParams["titolo"] = "Risulatati";
    $templateParams["nome"] = $_GET["nome"];

    require("template/Base.php");
?>