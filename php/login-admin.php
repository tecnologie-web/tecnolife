<?php
    require_once("bootstrap.php");

    if($_SESSION["amministratore"]==0){
        header("location: login.php");
    } else if($_SESSION["amministratore"]==1){
        $templateParams["prodotti"] = $dbh->getProducts();
        $templateParams["categorie"] = $dbh->getCategories();
        $templateParams["titolo"] = "Admin Home";
        $templateParams["nome"] = "template/Admin-Home.php";
        if(isset($_GET["formmsg"])){
            $templateParams["formmsg"] = $_GET["formmsg"];
        }
    }

    require "template/Base.php";
?>