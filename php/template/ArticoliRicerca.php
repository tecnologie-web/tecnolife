<section class="funzionalità">
    <div class="funzioni">   
        <a href="ordinamentoProdotti.php?action=1&ricerca=<?php echo $templateParams["ordinamento"];?>&nome=<?php echo $templateParams["nome"];?>">Ordina per prezzo crescente</a>
        <a href="ordinamentoProdotti.php?action=2&ricerca=<?php echo $templateParams["ordinamento"];?>&nome=<?php echo $templateParams["nome"];?>">Ordina per prezzo decrescente</a>
        <a href="ordinamentoProdotti.php?action=3&ricerca=<?php echo $templateParams["ordinamento"];?>&nome=<?php echo $templateParams["nome"];?>">Ordina per prodotti più venduti</a>
        <a href="ordinamentoProdotti.php?action=4&ricerca=<?php echo $templateParams["ordinamento"];?>&nome=<?php echo $templateParams["nome"];?>">Prodotti in offerta</A>
        <a href="ordinamentoProdotti.php?action=5&ricerca=<?php echo $templateParams["ordinamento"];?>&nome=<?php echo $templateParams["nome"];?>">Mostra tutti</a>
    </div>
    <div class="prod">
        <ul>
        <?php if(isset($templateParams["ricerca"])): ?>
        <?php foreach($templateParams["ricerca"] as $prodotto): ?>
            <li><a href="singoloArticolo.php?codProdotto=<?php echo $prodotto["codProdotto"];?>"><img alt="<?php echo $prodotto["nomeProdotto"];?>" src="<?php echo UPLOAD_DIR.$prodotto["imgProdotto"]; ?>" /><figcaption><?php echo $prodotto["nomeProdotto"];?><br><?php echo $prodotto["prezzoUnitario"];?>€</figcaption></a></li> 
        <?php endforeach; ?>  
        <?php endif;?> 
        </ul>
    </div>
    <?php if(isset($templateParams["msg"])):?>
    <div id="messaggio">
    <p><?php echo $templateParams["msg"]; ?></p>
    </div>
    <?php endif; ?>
</section>