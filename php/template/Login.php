<form action="login.php" method="POST">
    <h2>Accedi</h2>
    <ul>
        <li>
            <label for="username">Inserisci e-mail:</label><input type="text" id="username" name="username" />
        </li>
        <li>
            <label for="password">Inserisci password:</label><input type="password" id="password" name="password" />
        </li>
        <li>
            <input type="submit" name="submit" value="Continua" />
        </li>
    </ul>
        <?php if(isset($templateParams["login"]) && isset($_POST["username"]) && isset($_POST["password"])): ?>
        <div id="messaggio">
        <p><?php echo $templateParams["login"]; ?></p>
        </div>
        <?php endif; ?>
</form>

<section id="registrazione">
    <a href="accountCreation.php">Sei nuovo su Tecno Life ?</a>
</section>