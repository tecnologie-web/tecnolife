<!DOCTYPE html>
<html lang="it">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Tecno Life - <?php echo $templateParams["titolo"]; ?></title>
    <link rel="stylesheet" type="text/css" href="./css/style.css"/> 
</head>
<body>
    <div class="topnav">
        <a class="active" href="index.php" id="title">Tecno Life</a>
        <script>
            function validateForm() {
                const x = document.forms["ricerca"]["ricercaProd"].value;
                if (x == "") {
                    alert("Nota: non è stato digitato nulla sulla barra di ricerca");
                return false;
                }
            }
        </script>
        <form action="ricerca.php" method="GET" name="ricerca" onsubmit="return validateForm()">
        <input type="text" id="ricercaProd" name="ricercaProd" placeholder="Search..">
        <input type="submit" value="Cerca" class="cerca">
        </form>


        <a class="shopandlogin" id="log" href="login.php">Login</a>
        <a class="shopandlogin" href="carrello.php">Carrello</a>
    </div>
    <main>
        <?php require($templateParams["nome"]); ?>
    </main>
    <aside>
        <footer>
        <ul>
            <li>Chi siamo
            <p>
                Siamo Stefano e Riccardo, due studenti di Ingegneria Informatica che si sono cimentati nella realizzazione di questo portale e-commerce sul web. 
            </p>
            </li>
            <li>Diventa amministratore
            <p>
                Ciao! Se vuoi vendere o lavorare con noi inviaci una mail di richiesta. 
            </p>
            </li>
            <li>Contattaci presso i nostri principali social
                <div>
                    <a href="#"><img alt="Instagram" src="./upload/utils/Instagram.png"/></a>
                    <a href="#"><img alt="Facebook" src="./upload/utils/Facebook.png"/></a>
                </div>
            </li>
        </footer>
    </aside>
</body>
</html>
