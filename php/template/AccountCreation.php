<form action="accountCreation.php" method="POST">
    <h2>Crea il tuo account</h2>
    <ul>
        <li>
            <label for="username">Inserisci e-mail:</label><input type="text" id="username" name="username" />
        </li>
        <li>
            <label for="password">Inserisci password:</label><input type="password" id="password" name="password" />
        </li>
        <li>
            <input onClick="window.location.href='Login.html'" type="submit" Value="Conferma">
            <a href="login.php">Annulla</a>
        </li>
    </ul>
    <?php if(isset($templateParams["login"]) && isset($_POST["username"]) && isset($_POST["password"])): ?>
        <div id="messaggio">
        <p><?php echo $templateParams["login"]; ?></p>
        </div>
    <?php endif; ?>
</form>
