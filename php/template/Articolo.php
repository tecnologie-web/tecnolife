<section>
    <div class="articoli">
        <?php foreach($templateParams["singoloProdotto"] as $prodotto): ?>
        <img alt="<?php echo $prodotto["nomeProdotto"];?>" src="<?php echo UPLOAD_DIR.$prodotto["imgProdotto"]; ?>"/><figcaption><?php echo $prodotto["nomeProdotto"];?></figcaption>
        <div class="descrizione"><?php echo $prodotto["descrizione"];?> </div>
        <aside>Prezzo: <?php echo $prodotto["prezzoUnitario"];?>€
        <a href="inserimentoCarrello.php?codProdotto=<?php echo $prodotto["codProdotto"];?>&prezzo=<?php echo $prodotto["prezzoUnitario"];?>&nomeProdotto=<?php echo $prodotto["nomeProdotto"];?>&imgProdotto=<?php echo $prodotto["imgProdotto"];?>">Aggiungi al carrello</a>
        </aside>
        <?php endforeach; ?>     
    </div>
    <?php if(isset($templateParams["nelCarrello"])):?>
        <div id="messaggio">
            <p><?php echo $templateParams["nelCarrello"];?></p>
        </div>
    <?php endif; ?>
</section>