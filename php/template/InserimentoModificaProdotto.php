<section class="gestione">
<?php
    $azione = getAction($templateParams["azione"]);
?>

<form action="processa-prodotto.php" method="POST" enctype="multipart/form-data">
    <h2>Inserisci nuovo articolo</h2>
    <ul>
        <li>
            <label for="nomeProd">Nome prodotto:</label><input id="nomeProd" name="nomeProd" value="<?php if($templateParams["azione"]==2){ echo $templateParams["prodotto"][0]["nomeProdotto"];}?>"></input>
        </li>
        <li>
            <label for="descProd">Descrizione prodotto:</label><textarea id="descProd" name="descProd"><?php if($templateParams["azione"]==2){ echo $templateParams["prodotto"][0]["descrizione"];}?></textarea>
        </li>
        <li>
            <label for="prProd">Prezzo prodotto:</label><input type="text" id="prProd" name="prProd" value="<?php if($templateParams["azione"]==2){echo $templateParams["prodotto"][0]["prezzoUnitario"];}?>"/>
        </li>
        <li>
            <label for="qtaProd">Quantità disponibile:</label><input type="text" id="qtaProd" name="qtaProd" value="<?php if($templateParams["azione"]==2){echo $templateParams["prodotto"][0]["quantità"];}?>" />
        </li>
        <li>
            <label for="imgProd"><?php if($templateParams["azione"]==2){ echo "Re-Inserisci immagine prodotto:";} else { echo "Immagine prodotto: ";}?></label><input type="file" name="imgProd" id="imgProd"/>
        </li>
        <li>
            <label for="offProd">Prodotto in offerta:</label><input type="checkbox" id="offProd" name="offProd" <?php if($templateParams["azione"]==2){echo ($templateParams["prodotto"][0]["offerta"]==1 ? 'checked' : '');}?>>
        </li>
        <li>
           <label for="catProd">Selezionare una categoria:</label>
           <?php foreach($templateParams["categorie"] as $categoria): ?>
                <label id="nomeCat" for="categoria_<?php echo $categoria["codCategoria"];?>"><?php echo $categoria["nomeCategoria"];?></label>
                <input type="radio" id="categoria_<?php echo $categoria["codCategoria"]; ?>" name="categoria" value="<?php echo $categoria["codCategoria"]; ?>" <?php if($templateParams["azione"]==2){ echo ($templateParams["categoriaprodotto"][0]["categoria"]==$categoria["codCategoria"] ? 'checked' : '');   }  ?>>
            <?php endforeach; ?>
        </li>
        <li>
            <input type="submit" name="submit" value="<?php echo $azione?>" />
            <a href="login-admin.php">Annulla</a>
        </li>
    </ul>
    <input type="hidden" id="action" name="action" value="<?php echo $templateParams["azione"];?>" />
    <input type="hidden" id="codice" name="codice" value="<?php echo $templateParams["prodotto"][0]["codProdotto"];?>" />
</form>
</section>