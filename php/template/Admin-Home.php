<section class="gestione">
    <?php if(isset($templateParams["formmsg"])):?>
    <div id="messaggio">
    <p><?php echo $templateParams["formmsg"]; ?></p>
    </div>
    <?php endif; ?>

    <div class="log-home">
        <a href="gestione-prodotti.php?action=1">Inserisci un nuovo prodotto</a>
        <a href="index.php">Torna alla Home</a>
        <a href="login.php">Torna alla Home-Login</a>
    </div>
    <div id="login-home">
        <label id="selectCat"><br>Scegli una categoria di cui visualizzare i prodotti da modificare o cancellare:</label><div id="drop-menu">
            <ul id="menu">
                <li><a href="#">Categorie</a>
                    <ul>
                        <?php foreach($templateParams["categorie"] as $categoria): ?>
                            <li><a href="selectCat.php?cat=<?php echo $categoria["codCategoria"] ;?>"><?php echo $categoria["nomeCategoria"]; ?></a></li>
                        <?php endforeach;?>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    
    <?php if(!empty($templateParams["selezione"])): ?>   
    <div id="login-home">
    <table id="elenco">
        <tr id="elenco">
            <th id="elenco">Descrizione</th><th id="elenco">Immagine</th><th id="elenco">Azione</th>
        </tr>
        <?php foreach($templateParams["selezione"] as $prodotto): ?>
        <tr id="elenco">
            <td id="elenco"><?php echo $prodotto["nomeProdotto"]; ?></td>
            <td id="elenco"><img alt="" src="<?php echo UPLOAD_DIR.$prodotto["imgProdotto"]; ?>"/></td>
            <td id="elenco">
                <a href="gestione-prodotti.php?action=2&id=<?php echo $prodotto["codProdotto"]; ?>">Modifica</a>
                <a href="gestione-prodotti.php?action=3&id=<?php echo $prodotto["codProdotto"]; ?>" onclick="return confirm('Sicuro di voler eliminare il protodotto ?')">Cancella</a>
            </td>
        </tr>
        <?php endforeach; ?>
    </table>
    </div>
    <?php endif; ?> 

</section>