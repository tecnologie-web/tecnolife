<form action="concludiPagamento.php" method="POST">
    <?php if(isset($templateParams["msg"])):?>  
    <div id="messaggio">
    <p><?php echo $templateParams["msg"]; ?></p>
    </div>
    <?php endif; ?>
    <h2>Modulo di pagamento, totale dell'acquisto: <?php echo $templateParams["sommaTotale"];?>.00 €</h2>
    <ul>
        <li>
            <label for="NumeroCarta">Numero carta:</label><input type="text" id="NumeroCarta" name="NumeroCarta"/>
        </li>
        <li>
            <label for="Indirizzo">Indirizzo:</label><input type="text" id="Indirizzo" name="Indirizzo"/>
        </li>
        <li>
            <label for="informazioni">Eventuali informazioni:</label><textarea id="informazioni" name="informazioni"></textarea>
        </li>
        <li>
            <label for="PermessoMail">Acconsenti all'invio di e-mail di notifica:</label><input type="checkbox" id="PermessoMail" name="PermessoMail"/>
        </li>
        <li>
            <a id="back" href="carrello.php">Torna alla pagina precedente</a>
            <input id="back" type="submit" name="submit" value="Conferma acquisto"/> 
        </li>
        <input type="hidden" id="totale" name="totale" value="<?php echo $templateParams["sommaTotale"]; ?>"/>
    </ul>
</form>