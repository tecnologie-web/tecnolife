    <?php if(isset($templateParams["msg"])):?>  
    <div id="messaggio">
    <p><?php echo $templateParams["msg"]; ?></p>
    </div>
    <?php endif; ?>
    <section id="carrello">
    <h2>Articoli nel carrello: <?php echo count($templateParams["carrello"]);?> </h2>
    <form action="Acquisto.php" method="POST">
    <ul>
        <?php foreach($templateParams["carrello"] as $prodotto):?>
        <li>
        <?php if($prodotto["inclusione"]==1):?>
            <input type="checkbox" id="presente_<?php echo $prodotto["codProdotto"];?>" name="presente_<?php echo $prodotto["codProdotto"];?>"checked>
            <?php endif; if($prodotto["inclusione"]==0):?>
            <input type="checkbox" id="presente_<?php echo $prodotto["codProdotto"];?>" name="presente_<?php echo $prodotto["codProdotto"];?>">
        <?php endif;?>
        <div id="primo">
            <a href="singoloArticolo.php?codProdotto=<?php echo $prodotto["codProdotto"];?>"><img src="<?php echo UPLOAD_DIR.$prodotto["imgProdotto"]; ?>" alt=""/></a>
        </div>
        <div id="secondo">
            <label id="nome"><?php echo $prodotto["nomeProdotto"];?></label>
            <label id="rimuovi"><a href="rimuoviProdottoCarrello.php?codProdotto=<?php echo $prodotto["codProdotto"];?>"><input type="button" id="rimuoviProdotto" value="Rimuovi prodotto"></input></a></label>
            <label id="quantità" for="quantità_<?php echo $prodotto["codProdotto"];?>">Quantità:<input type="number" name="quantità_<?php echo $prodotto["codProdotto"];?>" value="<?php echo $prodotto["quantità"];?>" min="1"></label>
        </div>
        <div id="terzo">
            <label id="prezzo" for="prezzo">Prezzo: <?php echo $prodotto["prezzo"]; ?>.00€</label>
        </div>
        </li>
        <?php endforeach; ?>
    </ul>
    <div id="acquista">
      <input type="submit" id="carrello" name="submit" value="Acquista" />
    </div>
    </form>
</section>


