<div class="categories">
    <h2>Scegli tra le nostre categorie !</h2>
    <table>
        <tbody> 
            <tr>
             <?php foreach($templateParams["categorie"] as $categoria): ?>
                <td><a href="articoliPerCategoria.php?action=<?php echo $categoria["codCategoria"] ;?>"><img alt="<?php echo $categoria["nomeCategoria"]?>" src="./upload/utils/<?php echo $categoria["nomeCategoria"]; ?>.png"/><figcaption><?php echo $categoria["nomeCategoria"]?></figcaption></a></td>
              <?php endforeach;?>
            </tr>
        </tbody> 
    </table>
</div>
<section>
    <div class="prodotti">
        <h2>Best Sellers</h2>
        <ul>
          <?php foreach($templateParams["bestSellers"] as $prodotto): ?>
            <li><a href="singoloArticolo.php?codProdotto=<?php echo $prodotto["codProdotto"];?>"><img alt="<?php echo $prodotto["nomeProdotto"];?>" src="<?php echo UPLOAD_DIR.$prodotto["imgProdotto"]; ?>"/><figcaption><?php echo $prodotto["nomeProdotto"];?><br><?php echo $prodotto["prezzoUnitario"];?>€</figcaption></a></li> 
          <?php endforeach; ?>  
        </ul>
    </div>
    <div class="prodotti">
        <h2>Offerte di Natale</h2>
        <ul>
          <?php foreach($templateParams["offerte"] as $prodotto): ?>
            <li><a href="singoloArticolo.php?codProdotto=<?php echo $prodotto["codProdotto"];?>"><img alt="<?php echo $prodotto["nomeProdotto"];?>" src="<?php echo UPLOAD_DIR.$prodotto["imgProdotto"]; ?>"/><figcaption><?php echo $prodotto["nomeProdotto"];?><br><?php echo $prodotto["prezzoUnitario"];?>€</figcaption></a></li> 
          <?php endforeach; ?>   
        </ul>
    </div>
</section>