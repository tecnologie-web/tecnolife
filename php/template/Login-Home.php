<?php if(isset($templateParams["msg"])):?>  
    <div id="messaggio">
    <p><?php echo $templateParams["msg"]; ?></p>
    </div>
    <?php endif; ?>
<div class="log-home">
    <?php if($_SESSION["amministratore"]==1):?>
    <a href="login-admin.php">Admin Page</a>
    <?php endif;?>
    <a href="index.php">Torna alla Home</a>
    <a href="notifiche.php">Notifiche</a>
    <a href="userMods.php?logout=1" onclick="return confirm('Sicuro di voler uscire ?')">Logout</a>
    <a href="userMods.php?remove=1" onclick="return confirm('Sicuro di voler eliminare il tuo account ?')">Elimina Account</a>
</div>

<form action="userMods.php" method="POST">
    <h2>Modifica la e-mail</h2>
        <ul>
            <li>
                <label for="newMail">Inserisci nuova e-mail:</label><input type="text" id="newMail" name="newMail" />
            </li>
            <li>
                <label for="password">Conferma la tua password attuale:</label><input type="password" id="password" name="password" />
            </li>
            <li>
                <input type="submit" name="submit" value="Conferma" />
            </li>
        </ul>
</form>

<form action="userMods.php" method="GET">
    <h2>Modifica la password</h2>
        <ul>
            <li>
                <label for="newPsw">Inserisci nuova password:</label><input type="password" id="newPsw" name="newPsw" />
            </li>
            <li>
                <label for="password">Conferma la tua password attuale:</label><input type="password" id="oldPsw" name="oldPsw" />
            </li>
            <li>
                <input type="submit" name="submit" value="Conferma" />
            </li>
        </ul>
</form>
