<?php
require_once("bootstrap.php");
    

$templateParams["titolo"] = "Home";
$templateParams["nome"] = "template/HomePage.php";
$templateParams["offerte"] = $dbh->getProdOnOffer();
$templateParams["bestSellers"] = $dbh->getBestSellers();
$templateParams["categorie"] = $dbh->getCategories();
require("template/Base.php");
?>
