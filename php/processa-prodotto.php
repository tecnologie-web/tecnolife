<?php
require_once("bootstrap.php");

if(!isUserLoggedIn() || !isset($_POST["action"])){
    header("location: index.php");
}

if($_POST["action"] == 1){
   if(isset($_POST["nomeProd"]) && isset($_POST["descProd"]) && isset($_POST["prProd"]) && isset($_POST["qtaProd"])){
      if(!isset($_POST["offProd"])){
         $offProdotto= 0;
      }else{
         $offProdotto= $_POST["offProd"];
      }
      $nomeProd = $_POST["nomeProd"];
      $descProd= $_POST["descProd"];
      $prProdotto= $_POST["prProd"];
      $qtaProd = $_POST["qtaProd"];

      $categoria_inserita = $_POST["categoria"];

      list($result,$msg) = uploadImage(UPLOAD_DIR, $_FILES["imgProd"]);
      if($result == 1){
         $imgProd= $msg;

         $codProdotto = $dbh->insertProd($nomeProd, $prProdotto, $qtaProd, $descProd, $imgProd, $offProdotto);
         if($codProdotto != false){
            $dbh->insertProdCat($codProdotto, $categoria_inserita);
            $msg = "Inserimento avvenuto con successo !";
         } else {
               $msg = "Errore nell'inserimento !";
         }
      }
      header("location: login-admin.php?formmsg=".$msg);
   }
} else if($_POST["action"]==2){
   $codice = $_POST["codice"];
   $categorie = $dbh -> getCategories();
   $prodotto = $dbh -> getProduct($codice);
   $cat = $dbh -> getCategoryOfProduct($codice);

   if(isset($_POST["nomeProd"]) && isset($_POST["descProd"]) && isset($_POST["prProd"]) && isset($_POST["qtaProd"])){
      if(!isset($_POST["offProd"])){
         $offProdotto= 0;
      }else{
         $offProdotto= $_POST["offProd"];
      }
      $nomeProd = $_POST["nomeProd"];
      $descProd= $_POST["descProd"];
      $prProdotto= $_POST["prProd"];
      $qtaProd = $_POST["qtaProd"];
   
   list($result,$msg) = uploadImage(UPLOAD_DIR, $_FILES["imgProd"]);
      if($result == 1){
         $imgProd= $msg;
         if($prodotto[0]["nomeProdotto"] != $nomeProd){
            $dbh->setProductName($nomeProd, $prodotto[0]["codProdotto"]);
         }
         if($prodotto[0]["offerta"] != $offProdotto){
            $dbh->setProductOffer($offProdotto, $prodotto[0]["codProdotto"]);
         }
         if($prodotto[0]["descrizione"] != $descProd){
            $dbh->setProductDescription($descProd, $prodotto[0]["codProdotto"]);
         }
         if($prodotto[0]["prezzoUnitario"] != $prProdotto){
            $dbh->setProductPrice($prProdotto, $prodotto[0]["codProdotto"]);
         }
         if($prodotto[0]["quantità"] != $qtaProd){
            $dbh->setProductQuantity($qtaProd, $prodotto[0]["codProdotto"]);
         }
         $dbh->setProductImage($msg, $prodotto[0]["codProdotto"]);

         $categoria_inserita = $_POST["categoria"];
         $dbh->setProductCategory($categoria_inserita, $prodotto[0]["codProdotto"]);
         $msg = "Modifica avvenuta con successo !";
      }
      header("location: login-admin.php?formmsg=".$msg);
   }
}
?>