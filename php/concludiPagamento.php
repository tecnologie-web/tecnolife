<?php
require_once("bootstrap.php");

/*TO USE MAIL SENDER:
*DOWNLOAD PHP MAILER: https://github.com/PHPMailer/PHPMailer
*YOU NEED TO CREATE APP PASSWORD IF YOU USE GMAIL TO SEND MAILS: https://support.google.com/accounts/answer/185833?p=InvalidSecondFactor&visit_id=637442395919051212-2324565872&rd=1
*EXTRACT IT AND COPY:  SMTP.php, PHPMailer.php and Exception.php (THAT ARE INSIDE SRC FOLDER), TO xamppfiles/htdocs/lib/php/
*THEN COPY THE FOLLOWING COMMANDS AND THE COMMANDS FROM LINE 36 TO LINE 65:
*/
require_once('SMTP.php');
require_once('PHPMailer.php');
require_once('Exception.php');
use \PHPMailer\PHPMailer\PHPMailer;
use \PHPMailer\PHPMailer\Exception;
$mail=new PHPMailer(true);


$invioMail = 0;
$msg = "";

if(isset($_POST["NumeroCarta"]) && isset($_POST["Indirizzo"]) && $_POST["NumeroCarta"]!="" && $_POST["Indirizzo"]!=""){
    if(isset($_POST["PermessoMail"]) && $_POST["PermessoMail"]=="on"){
        $invioMail = 1;
    }
} else {
    $msg .= "Per favore inserisci almeno Numero carta e Indirizzo per completare l'acquisto !";
}
if($msg != ""){
    header("location: Pagamento.php?msg=".$msg."&tot=".$_POST["totale"]);
} else {
    $username = $dbh->getUserMail($_SESSION["idUtente"]);
    $nome = explode('@', $username[0]["username"]);
    if($invioMail==1){
        try {
            $mail->SMTPDebug=0;
            $mail->isSMTP();
            $mail->Host='smtp.gmail.com';
            $mail->SMTPAuth=true;
            $mail->Username='riccardo.bacca@gmail.com';
            $mail->Password='xhwphxgeeltqavgs';
            $mail->SMTPSecure='ssl';
            $mail->Port=465;
        
            $mail->setFrom('noreply@tecnolife.com', 'tecno life support');

            $mail->addAddress(' '.$username[0]["username"], 'optional recipient name');

            $mail->isHTML(true);
            $mail->Subject='Conferma acquisto presso Tecno Life';
            $mail->Body='Ciao, '.$nome[0].' ti ringraziamo per aver completato il tuo acquisto presso il nostro portale on-line. Inoltre ti confermiamo che gli oggetti da te acquistati per un totale di '.$_POST["totale"].' Euro sono stati ritirati dal corriere e verranno spediti all indirizzo da te scelto: '.$_POST["Indirizzo"].'<br><br><br>Questa mail è inviata automaticamente dal sistema.<br>Ti chiediamo percio di non rispondere in quanto la risposta non verra consegnata.';
            $mail->AltBody='';

            $mail->send();
            echo '<script>alert("Message has been sent !")</script>';    
        } 
        catch(Exception $e) {
            echo '<script>alert("Message could not be sent, Mailer Error: ".$mail->ErrorInfo)</script>';
        }
    }
    $dbh->insertUserNotify($_SESSION["idUtente"], 1, 'Conferma acquisto presso Tecno Life, Ciao, '.$nome[0].' ti ringraziamo per aver completato il tuo acquisto presso il nostro portale on-line. Inoltre ti confermiamo che gli oggetti da te acquistati per un totale di '.$_POST["totale"].' Euro sono stati ritirati dal corriere e verranno spediti all indirizzo da te scelto: '.$_POST["Indirizzo"]);

    $templateParams["carrello"] = $dbh->getCarrello($_SESSION["idUtente"]);
    foreach($templateParams["carrello"] as $prodottoInCarrello){
        if($prodottoInCarrello["inclusione"]==1){
            $prodotto = $dbh->getProduct($prodottoInCarrello["codProdotto"]);
            $quantitàResidua = $prodotto[0]["quantità"]-$prodottoInCarrello["quantità"];
            $dbh->setProductQuantity($quantitàResidua, $prodottoInCarrello["codProdotto"]);
            $prodotto = $dbh->getProduct($prodottoInCarrello["codProdotto"]);
            if($prodotto[0]["quantità"]==0){
                $amministratori = $dbh->getAdministratorsId();
                foreach($amministratori as $amministratore){
                    try {
                        $mail->SMTPDebug=0;
                        $mail->isSMTP();
                        $mail->Host='smtp.gmail.com';
                        $mail->SMTPAuth=true;
                        $mail->Username='riccardo.bacca@gmail.com';
                        $mail->Password='xhwphxgeeltqavgs';
                        $mail->SMTPSecure='ssl';
                        $mail->Port=465;
                    
                        $mail->setFrom('noreply@tecnolife.com', 'tecno life support');
                    
                        $mail->addAddress(' '.$amministratore["username"], 'optional recipient name');
                    
                        $mail->isHTML(true);
                        $mail->Subject='Sono finite le scorte di un prodotto !';
                        $mail->Body='Ti informiamo che le scorte del prodotto: '.$prodotto[0]["nomeProdotto"].' sono attualmente finite !<br>Il codice identificativo è: '.$prodotto[0]["codProdotto"].'<br><br><br>NON RISPONDERE A QUESTA E-MAIL';
                        $mail->AltBody='';
            
                        $mail->send(); 
                    } 
                    catch(Exception $e) {}
                    $dbh->insertUserNotify($amministratore["idUtente"], 1, 'Sono finite le scorte di un prodotto !<br>Ti informiamo che le scorte del prodotto: '.$prodotto[0]["nomeProdotto"].' sono attualmente finite !<br>Il codice identificativo e`: '.$prodotto[0]["codProdotto"]);
                }
            }
            $dbh->deleteFromCarrello($prodottoInCarrello["codProdotto"], $_SESSION["idUtente"]);
        }
    }

    $templateParams["nome"] = "template/EndingPage.php";
    $templateParams["titolo"] = "Ringraziamento";
    require "template/Base.php";
}
?>






