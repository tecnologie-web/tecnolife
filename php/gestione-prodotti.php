<?php
    require_once("bootstrap.php");

    if(!isUserLoggedIn()){
        header("location: login.php");
     }
    if($_GET["action"] == 1){
        $templateParams["titolo"] = "Inserisci articolo";
        $templateParams["nome"] = "InserimentoModificaProdotto.php";
        $templateParams["categorie"] = $dbh->getCategories();

        $templateParams["azione"] = $_GET["action"];
    }
    if($_GET["action"] == 2){
        $templateParams["titolo"] = "Modifica articolo";
        $templateParams["nome"] = "InserimentoModificaProdotto.php";
        $templateParams["categorie"] = $dbh->getCategories();
        $templateParams["prodotto"] = $dbh->getProduct($_GET["id"]);
        $templateParams["categoriaprodotto"] = $dbh->getCategoryOfProduct($_GET["id"]);

        $templateParams["azione"] = $_GET["action"];
    }
    if($_GET["action"] == 3){
        $dbh->deleteCategoriesOfproduct($_GET["id"]);
        $dbh->deleteProduct($_GET["id"]);
        $msg = "Prodotto eliminato con successo !";
        header("location: login-admin.php?formmsg=".$msg);
    }
    
    require("template/Base.php");
?>