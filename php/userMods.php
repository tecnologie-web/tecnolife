<?php
require_once("bootstrap.php");

$msg = "";
$psw = $dbh->getUserPsw($_SESSION["idUtente"]);

if(isset($_GET["logout"]) && $_GET["logout"]==1){
    unset($_SESSION["idUtente"]);
    unset($_SESSION["username"]);
    unset($_SESSION["amministratore"]);
    header("location: index.php");
} else if(isset($_POST["newMail"]) && $_POST["newMail"]!=""){
    if(isset($_POST["password"]) && $_POST["password"]==$psw[0]["password"]){
        $dbh->setUserMail($_POST["newMail"], $_SESSION["idUtente"]);
    } else {
        $msg .= "Password non corretta, riprova !";
    }
    if($msg == ""){
        $msg="Username modificato con successo !";
    }
    header("location: login.php?msg=".$msg);
} else if(isset($_GET["newPsw"]) && $_GET["newPsw"]!=""){
    if(isset($_GET["oldPsw"]) && $_GET["oldPsw"]==$psw[0]["password"]){
        $dbh->setUserPsw($_GET["newPsw"], $_SESSION["idUtente"]);
    } else {
        $msg .= "Password non corretta, riprova !";
    }
    if($msg == ""){
        $msg="Password modificata con successo !";
    }
    header("location: login.php?msg=".$msg);
} else {
    if(isset($_GET["remove"]) && $_GET["remove"]==1){
        $dbh->removeUser($_SESSION["username"], $_SESSION["idUtente"]);
        $dbh->removeUserShoppingCart($_SESSION["idUtente"]);
        unset($_SESSION["idUtente"]);
        unset($_SESSION["username"]);
        unset($_SESSION["amministratore"]);
        header("location: index.php");
    }
}



?>