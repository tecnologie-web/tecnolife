<?php
    require_once("bootstrap.php");
    if(!isset($_GET["action"])){
        header("location : index.php");
    }else{
        $templateParams["prodotti"] = $dbh->getProdByCategories($_GET["action"]);
        $templateParams["titolo"] = "Articoli per categoria";
        $templateParams["nome"] = "template/Articoli.php";
        $temp = $dbh->getNameCategory(intval($_GET["action"]));
        $templateParams["ordinamento"] = $temp[0];
    }
    
    require("template/Base.php");
?>