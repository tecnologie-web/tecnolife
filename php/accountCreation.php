<?php
require_once("bootstrap.php");
$templateParams["titolo"] = "Registrazione";
$templateParams["nome"] = "template/AccountCreation.php";
if(isset($_GET["msg"])){
    $templateParams["msg"] = $_GET["msg"];
}

if(isset($_POST["username"]) && isset($_POST["password"])){
    $pass = $_POST["password"];
    if(strlen($pass)<6){
        $templateParams["login"]="Errore: Utilizzare una password con almeno 6 caratteri !";
    } else if(!filter_var($_POST["username"], FILTER_VALIDATE_EMAIL)){
        $templateParams["login"]="Errore: Inserire una e-mail valida per l'account !";
    } else{
        $username = $dbh->getUsername($_POST["username"]);
        if(empty($username)){
            $dbh->registerUser($_POST["username"], $_POST["password"]);
            $login_result = $dbh->checkLogin($_POST["username"], $_POST["password"]);
            if(count($login_result)==0){
                $templateParams["login"] = "Credenziali errate!";
            } else {
                registerLoggedUser($login_result[0]);
            }
            $templateParams["nome"] = "template/HomePage.php";
            $templateParams["titolo"] = "Home";
            $templateParams["offerte"] = $dbh->getProdOnOffer();
            $templateParams["bestSellers"] = $dbh->getBestSellers();
            $templateParams["categorie"] = $dbh->getCategories();
        }
        else{
            $templateParams["login"] = "Errore: Username già esistente!";
        }
    }
}

require("template/Base.php");

?>