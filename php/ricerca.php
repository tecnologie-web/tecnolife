<?php
    require_once("bootstrap.php");
    
    if(!empty($_GET["ricercaProd"])){
        $templateParams["ordinamento"]= $_GET["ricercaProd"];
        $categoria= $dbh->getCategory($_GET["ricercaProd"]);
        if(isset($categoria)){
            $templateParams["ricerca"] = $dbh->getProdByCategories($categoria[0]);
        }else{
            $templateParams["ricerca"] = $dbh->getProductSearch($_GET["ricercaProd"]);
        }
        $templateParams["titolo"] = "Risultati ricerca";
        $templateParams["nome"] = "template/ArticoliRicerca.php";
        if(count($templateParams["ricerca"])==0){
            $templateParams["msg"] = "Nessun risultato trovato per la ricerca !";
        }
    }
    require("template/Base.php");
?>