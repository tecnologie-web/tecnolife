<?php
    require_once("bootstrap.php");
    
    if(isUserLoggedIn()) {
        unset($_SESSION["nextCarrello"]);
        $templateParams["carrello"] = $dbh->getCarrello($_SESSION["idUtente"]);
        $templateParams["nome"] = "template/Carrello.php";
        $templateParams["titolo"] = "Carrello";
        if(isset($_GET["msg"])){
            $templateParams["msg"] = $_GET["msg"];
        }
    } else {
        $_SESSION["nextCarrello"] = 1;
        header("Location: login.php");
    }

    require("template/Base.php");
?>