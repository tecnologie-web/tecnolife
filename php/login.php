<?php
    require_once("bootstrap.php");

    if(isUserLoggedIn()){
        $templateParams["nome"] = "template/Login-Home.php";
        $templateParams["titolo"] = "Login Home";
        if(isset($_GET["msg"])){
            $templateParams["msg"] = ($_GET["msg"]);
        }
    } else {
        if(isset($_POST["username"]) && isset($_POST["password"])){
            $login_result = $dbh->checkLogin($_POST["username"], $_POST["password"]);
            if(count($login_result)==0){
                $templateParams["login"] = "Credenziali errate!";
            } else {
                registerLoggedUser($login_result[0]);
            }
        }
        $templateParams["nome"] = "template/Login.php";
        $templateParams["titolo"] = "Login";

        if(isUserLoggedIn()){
            if(isset($_SESSION["nextCarrello"]) && $_SESSION["nextCarrello"]==1){
                header("Location: carrello.php");
            } else {
                $templateParams["nome"] = "template/Login-Home.php";
                $templateParams["titolo"] = "Login Home";
            }
        }
    }
    require("template/Base.php");
?>