<?php
class DatabaseHelper{
    private $db;

    public function __construct($servername, $username, $password, $dbname, $port){
        $this->db = new mysqli($servername, $username, $password, $dbname, $port);
        if ($this->db->connect_error) {
            die("Connection failed: " . $this->db->connect_error);
        }        
    }

    /*********************LOGIN*****************************/
    public function checkLogin($username, $password){
        $query = "SELECT idUtente, username, amministratore  FROM utente WHERE username = ? AND password = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ss',$username, $password);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    /************************HOME-PAGE*******************/

    public function getProdOnOffer(){
        $query = "SELECT codProdotto, prezzoUnitario, nomeProdotto, imgProdotto FROM prodotto WHERE offerta=1";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getBestSellers(){
        $query = "SELECT codProdotto, prezzoUnitario, nomeProdotto, imgProdotto FROM prodotto WHERE quantità <= 40";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getCategories(){
        $query = "SELECT codCategoria, nomeCategoria FROM categoria";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    /**********************ADMIN-PAGE******************/

    public function insertProd($nomeProdotto, $prezzoUnitario, $quantità, $descrizione, $imgProdotto, $offerta){
        $query = "INSERT INTO prodotto (nomeProdotto, prezzoUnitario, quantità, descrizione, imgProdotto, offerta) VALUES (?, ?, ?, ?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('siissi',$nomeProdotto, $prezzoUnitario, $quantità, $descrizione, $imgProdotto, $offerta);
        $stmt->execute();      
        return $stmt->insert_id;
    }

    public function insertProdCat($codProdotto, $codCat){
        $query = "INSERT INTO prodotto_ha_categoria (prodotto, categoria) VALUES (?,?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii',$codProdotto, $codCat);   
        return $stmt->execute();
    }

    public function deleteCategoriesOfproduct($prodotto){
        $query = "DELETE FROM prodotto_ha_categoria WHERE prodotto = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$prodotto);
        return $stmt->execute();
    }

    public function deleteProduct($codProdotto){
        $query = "DELETE FROM prodotto WHERE codProdotto = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$codProdotto);
        return $stmt->execute(); 
    }

    public function setProductOffer($offerta, $codProdotto){
        $query = "UPDATE prodotto SET offerta = ? WHERE codProdotto = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii',$offerta, $codProdotto);
        return $stmt->execute();
    }

    public function setProductDescription($descrizione, $codProdotto){
        $query = "UPDATE prodotto SET descrizione = ? WHERE codProdotto = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('si',$descrizione, $codProdotto);
        return $stmt->execute();
    }

    public function setProductPrice($prezzo, $codProdotto){
        $query = "UPDATE prodotto SET prezzoUnitario = ? WHERE codProdotto = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii',$prezzo, $codProdotto);
        return $stmt->execute();
    }

    public function setProductQuantity($qta, $codProdotto){
        $query = "UPDATE prodotto SET quantità = ? WHERE codProdotto = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii',$qta, $codProdotto);
        return $stmt->execute();
    }

    public function setProductCategory($cat, $codProdotto){
        $this->deleteCategoriesOfproduct($codProdotto);
        $this->insertProdCat($codProdotto, $cat);
    }

    public function setProductImage($msg, $codProdotto){
        $query = "UPDATE prodotto SET imgProdotto = ? WHERE codProdotto = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('si',$msg, $codProdotto);
        return $stmt->execute();
    }

    public function setProductName($name, $codProdotto){
        $query = "UPDATE prodotto SET nomeProdotto = ? WHERE codProdotto = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('si',$name, $codProdotto);
        return $stmt->execute();
    }

    public function getCategoryOfProduct($codice){
        $query = "SELECT categoria FROM prodotto_ha_categoria WHERE prodotto = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$codice);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getProducts(){
        $query = "SELECT * FROM prodotto";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    
    /**********************CARRELLO*********************/

    public function getCarrello($idUtente){
        $query = "SELECT * FROM carrello WHERE idUtente = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$idUtente);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getInclusione($idUtente){
        $query = "SELECT * FROM carrello WHERE idUtente = ? AND inclusione = 1";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$idUtente);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function insertProductOnCarrell($codProdotto, $prezzo, $nomeProdotto, $imgProdotto, $idUtente){
        $query = "INSERT INTO carrello (codProdotto, prezzo, nomeProdotto, imgProdotto, quantità, inclusione, idUtente) VALUES (?,?,?,?,1,1,?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('iissi',$codProdotto, $prezzo, $nomeProdotto,$imgProdotto, $idUtente);
        $stmt->execute();      
        return $stmt->insert_id;
    }

    public function getProductOnCarrel($codProdotto, $idUtente){
        $query = "SELECT codProdotto FROM carrello WHERE codProdotto = ? AND idUtente = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('si',$codProdotto, $idUtente);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC); 
    }

    public function deleteFromCarrello($codProdotto, $idUtente){
        $query = "DELETE FROM carrello WHERE codProdotto = ? AND idUtente = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii',$codProdotto, $idUtente);
        return $stmt->execute();
    }


    /*********************RICERCA************************/

    public function getCategory($nomeCategoria){
        $query = "SELECT codCategoria FROM categoria WHERE nomeCategoria LIKE ?";
        $stmt = $this->db->prepare($query);
        $nomeCategoria = '%'. $nomeCategoria . '%';
        $stmt->bind_param('s',$nomeCategoria);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_row();
    }

    public function getNameCategory($codCat){
        $query = "SELECT nomeCategoria FROM categoria WHERE codCategoria = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$codCat);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_row();
    }

    public function getProdByCategories($categoria){ 
        $query = "SELECT * FROM prodotto INNER JOIN prodotto_ha_categoria ON prodotto.codProdotto = prodotto_ha_categoria.prodotto WHERE prodotto_ha_categoria.categoria = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s',$categoria);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getProductSearch($ricerca){
        $query = "SELECT codProdotto, prezzoUnitario, nomeProdotto, imgProdotto FROM prodotto WHERE nomeProdotto LIKE ?";
        $stmt = $this->db->prepare($query);
        $ricerca =  '%' . $ricerca . '%';
        $stmt->bind_param("s", $ricerca);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function orderASCPrice($ricerca){
        $query = "SELECT codProdotto, prezzoUnitario, nomeProdotto, imgProdotto FROM prodotto WHERE nomeProdotto LIKE ? ORDER BY prezzoUnitario ASC";
        $stmt = $this->db->prepare($query);
        $ricerca = '%' . $ricerca . '%';
        $stmt->bind_param("s", $ricerca);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function orderASCPriceByCategory($categoria){
        $query = "SELECT * FROM prodotto INNER JOIN prodotto_ha_categoria ON prodotto.codProdotto = prodotto_ha_categoria.prodotto WHERE prodotto_ha_categoria.categoria = ? ORDER BY prezzoUnitario ASC";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s',$categoria);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function orderDESCPrice($ricerca){
        $query = "SELECT codProdotto, prezzoUnitario, nomeProdotto, imgProdotto FROM prodotto WHERE nomeProdotto LIKE ? ORDER BY prezzoUnitario DESC";
        $stmt = $this->db->prepare($query);
        $ricerca = '%' . $ricerca . '%';
        $stmt->bind_param('s',$ricerca);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function orderDESCPriceByCategory($categoria){
        $query = "SELECT * FROM prodotto INNER JOIN prodotto_ha_categoria ON prodotto.codProdotto = prodotto_ha_categoria.prodotto WHERE prodotto_ha_categoria.categoria = ? ORDER BY prezzoUnitario DESC";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s',$categoria);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function orderByBestSellers($ricerca){
        $query = "SELECT codProdotto, prezzoUnitario, nomeProdotto, imgProdotto FROM prodotto WHERE nomeProdotto LIKE ? ORDER BY quantità ASC";
        $stmt = $this->db->prepare($query);
        $ricerca =  '%' . $ricerca . '%';
        $stmt->bind_param("s", $ricerca);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function orderByBestSellersCategory($categoria){
        $query = "SELECT * FROM prodotto INNER JOIN prodotto_ha_categoria ON prodotto.codProdotto = prodotto_ha_categoria.prodotto WHERE prodotto_ha_categoria.categoria = ? ORDER BY quantità ASC";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s',$categoria);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getProductOnOffer($ricerca){
        $query = "SELECT codProdotto, prezzoUnitario, nomeProdotto, imgProdotto FROM prodotto WHERE nomeProdotto LIKE ? AND offerta = 1";
        $stmt = $this->db->prepare($query);
        $ricerca =  '%' . $ricerca . '%';
        $stmt->bind_param("s", $ricerca);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);

    }

    public function getProductOnOfferByCategory($categoria){
        $query = "SELECT * FROM prodotto INNER JOIN prodotto_ha_categoria ON prodotto.codProdotto = prodotto_ha_categoria.prodotto WHERE prodotto_ha_categoria.categoria = ? AND offerta=1";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s',$categoria);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);

    }

    /********************SINGOLO-ARTICOLO*****************/

    public function getProduct($codProdotto){
        $query = "SELECT * FROM prodotto WHERE codProdotto = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$codProdotto);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    /*******************ACCOUNT-CREATION*****************/
    public function registerUser($username, $password){
        $query = "INSERT INTO utente (username, password, amministratore) VALUES (?,?,0)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ss',$username, $password);
        $stmt->execute();      
        return $stmt->insert_id;
    }

    public function getUsername($username){
        $query = "SELECT username FROM utente WHERE username = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s',$username);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }


    /****************USER-MOD***************************/

    public function getUserPsw($idUtente){
        $query = "SELECT password FROM utente WHERE idUtente = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $idUtente);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC); 
    }

    public function setUserMail($mail, $idUtente){
        $query = "UPDATE utente SET username = ? WHERE idUtente = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('si',$mail, $idUtente);
        return $stmt->execute();
    }

    public function setUserPsw($psw, $idUtente){
        $query = "UPDATE utente SET password = ? WHERE idUtente = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('si',$psw, $idUtente);
        return $stmt->execute();
    }

    public function removeUser($username, $idUtente){
        $query = "DELETE FROM utente WHERE username = ? AND idUtente = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('si',$username, $idUtente);
        return $stmt->execute();   
    }

    public function removeUserShoppingCart($idUtente){
        $query = "DELETE FROM carrello WHERE idUtente = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$idUtente);
        return $stmt->execute();   
    }

    /********************NOTIFICHE-USER/ADMIN************/
    public function insertUserNotify($idUtente, $tipologia, $contenuto){
        $query = "INSERT INTO notifiche (idUtente, tipologia, contenuto) VALUES (?,?,?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('iis',$idUtente, $tipologia, $contenuto);
        $stmt->execute();
        return $stmt->insert_id;
    }

    public function deleteUserNotify($idUtente, $codice){
        $query = "DELETE FROM notifiche WHERE idUtente = ? AND codice = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii',$idUtente, $codice);
        return $stmt->execute();      
    }

    public function getUserNotify($idUtente){
        $query = "SELECT * FROM notifiche WHERE idUtente = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$idUtente);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    /************************PAGAMENTO CARRELLO************/
    public function setIncludedShoppingCart($codice, $idUtente){
        $query = "UPDATE carrello SET inclusione = 1 WHERE codProdotto = ? AND idUtente = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii',$codice, $idUtente);
        return $stmt->execute();
    }

    public function setNotIncludedShoppingCart($codice, $idUtente){
        $query = "UPDATE carrello SET inclusione = 0 WHERE codProdotto = ? AND idUtente = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii',$codice, $idUtente);
        return $stmt->execute();
    }

    public function setQuantityShoppingCart($codice, $quantità, $idUtente){
        $query = "UPDATE carrello SET quantità = ? WHERE codProdotto = ? AND idUtente = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('iii',$quantità ,$codice, $idUtente);
        return $stmt->execute();
    }

    public function getUserMail($idUtente){
        $query = "SELECT username FROM utente WHERE idUtente = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $idUtente);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC); 
    }
 
    public function getAdministratorsId(){
        $query = "SELECT idUtente, username FROM utente WHERE amministratore = 1";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

}
?>