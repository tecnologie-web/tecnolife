<?php
    require_once("bootstrap.php");
    $dbh->deleteFromCarrello($_GET["codProdotto"], $_SESSION["idUtente"]);
    $templateParams["carrello"] = $dbh->getCarrello($_SESSION["idUtente"]);
    $templateParams["nome"] = "template/Carrello.php";
    $templateParams["titolo"] = "Carrello";
    require("template/Base.php");
?>