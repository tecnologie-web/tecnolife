<?php
    require_once("bootstrap.php");
    
    if(isUserLoggedIn()) {
        $onCarrell = $dbh->getProductOnCarrel($_GET["codProdotto"], $_SESSION["idUtente"]);
        
        if(empty($onCarrell)){
             $dbh->insertProductOnCarrell($_GET["codProdotto"], $_GET["prezzo"], $_GET["nomeProdotto"],$_GET["imgProdotto"], $_SESSION["idUtente"]);
             $templateParams["nelCarrello"]= "Articolo inserito nel carrello !";
        }else{
            $templateParams["nelCarrello"]= "Questo articolo è già nel carrello!";
        }
        
    } else {
        header("location: login.php");
    }
    $templateParams["singoloProdotto"] = $dbh->getProduct($_GET["codProdotto"]);
    $templateParams["titolo"] = "Articolo";
    $templateParams["nome"] = "template/Articolo.php";
    require("template/Base.php");
?>